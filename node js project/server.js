const express = require("express");
const mysql = require("mysql");
const bodyParser = require("body-parser")
const {
   check,
   validationResult
} = require('express-validator');


const app = express();
const port = 3000;
// const conn = require("./connection");
// console.log('check', conn);

const con = mysql.createConnection({
   host: "localhost",
   user: "root",
   password: "",
   database: "formdb"
})
con.connect((err) => {
   if (err) throw err;
   else {
      console.log("coonnection with database successfuully created");
   }
})



// configuratioon
app.set("view engine", "hbs");
app.set('views', "./view");
app.use(express.static(__dirname + "/public"))
// app.use(bodyParser.urlencoded({
//     extended:true
// }));
app.use(express.json());
app.use(express.urlencoded({
   extended: true
}));
// var helpers = require('handlebars-helpers')();

// helpers.registerHelper('if_eq', function(a, b, opts) {
//     if(a == b) // Or === depending on your needs
//         return opts.fn(this);
//     else
//         return opts.inverse(this);
// });

// Routing
app.get("/", (req, res) => {
   res.render("index");

})
app.get("/add", (req, res) => {
   res.render("register2");

})

app.get("/search", (req, res) => {
   res.render("login");

})
app.get("/update", (req, res) => {
   res.render("register");

})
app.get("/view", (req, res) => {
   res.render("google");

})
app.post("/adduser", [
   check('firstname', 'Name is req').isLength({
      min: 8
   }).withMessage('minimum length should be 8'),
   check('password', 'password is req').isLength({
      min: 8
   }).withMessage('the minimum length should be 8')
   .matches('[0-9]').withMessage('must have one digit')
   .matches('[A-Z]').withMessage('must conatin an Uppercase')
   .matches(/(?=.*?[a-z])/).withMessage('At least one Lowercase')
   .matches(/(?=.*?[#?!@$%^&*-])/).withMessage('At least one special character')
   .not().matches(/^$|\s+/).withMessage('White space not allowed'),
   check('gender').notEmpty().withMessage('gender required'),
   check('district').notEmpty().withMessage('district required'),
   check('state').notEmpty().withMessage('state required'),
   check('country').notEmpty().withMessage('country required'),
   check('address').notEmpty().withMessage('address required'),
   check('fpin').notEmpty().withMessage('pin required'),
   check('email').notEmpty().withMessage('Email Address required').normalizeEmail().isEmail().withMessage('must be a valid email')
], (req, res) => {

   try {
      // console.log('chekc', req.body);   
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
         // return res.status(400).json({ errors: errors.array() });
         res.render("register2", {
            errors: errors.array()
         });
      } else {
         const name = req.body.firstname;
         const email = req.body.email;
         const phone = req.body.number;
         const password = req.body.password;
         const gender = req.body.gender;
         const pin = req.body.fpin;
         const district = req.body.district;
         const state = req.body.state;
         const country = req.body.country;
         const address = req.body.address;
         // console.log(name);
         // console.log(email);
         // console.log(phone);
         // console.log(gender);
         // console.log(password);
         // console.log(gender);

         let query1 = `SELECT * from registration_form where email = "${email}"`;

         con.query(query1, (err, result) => {
            if (err) throw err;
            else {
               if (result.length > 0) {
                  console.log("email already exist");
                  res.render('register2');

               } else {
                  var sql = `INSERT INTO registration_form(name,email,phone,password,gender,pin,district,state,country,address)VALUES("${name}","${email}","${phone}","${password}","${gender}","${pin}","${district}","${state}","${country}","${address}")`;
                  console.log('q', sql);
                  con.query(sql, (err, result) => {
                     if (err) throw err;
                     console.log("registered successfully");
                     // alert("Registered successfuully");
                     res.render("login");

                  });

               }
            }
         })
      }
   } catch (e) {

   }

   // var sql = `INSERT INTO registration_form(name,email,phone,password,gender,pin,district,state,country,address)VALUES("${name}","${email}","${phone}","${password}","${gender}","${pin}","${district}","${state}","${country}","${address}")`;
   // console.log('q', sql);
   // con.query(sql, (err, result) => {
   //  if (err) throw err;
   //  console.log("registered successfuully");
   //  // alert("Registered successfuully");
   //   res.render("login");

   // });



})

app.post('/selectuser', (req, res) => {
   try {
      const email = req.body.email;
      const password = req.body.password;
      console.log(email);
      console.log(password);
      if (email && password) {
         let query2 = `SELECT * from registration_form where  email ="${email}" and password ="${password}"`;
         con.query(query2, (err, result) => {
            if (err) throw err;
            else {
               if (result.length > 0) {
                  console.log("registererd successfully");
                  // res.status(200).send({ status: true, msg:"registered successfully" });
                  res.render("login",{
                     status: true,
                     msg: "login successfully "
                  });

               } else {
                  console.log("invalid id and password");
                  // res.status(401).send({ status: false, msg: "invalid id and password" });
                  res.render("login", {
                     status: false,
                     msg: "invalid id and password"
                  });
               }
            }
         })
      } else {
         console.log("enter username and password");
         // res.status(403).send({ status: false, msg: "enter username and password" });
         res.render("login", {
            status: false,
            msg: "enter username and password"
         });
      }
   } catch (e) {
      // res.status(400).send({
      //    status: false,
      //    err: e
      // });
      res.render("login", {
         status: false,
         msg: e
      });
   }
})



// Add user data
// app.get("/adduser", (req, res) => {
//     // res.send(req.query);
//    const name = req.query.name;
//    const email = req.query.email;
//    const phone = req.query.phone;
//    const gender = req.query.gender;
//    const address = req.query.address;
//    console.log(name);
//    var sql = "INSERT INTO registration_form ( name, email, phone,gender,address) VALUES ( )";  
// con.query(sql, function (err, result) {  
// if (err) throw err;  
// console.log("1 record inserted");  
// });  

// const { name, email, phone, gender, address } = req.query;

// let query1 = "select * from registration_form where phone_no=? or email =?";
// conn.query(query1, [email, phone], (err, result) => {
//    if (err) throw err;
//    else {
//        res.send(result)

//       if (result.length > 0) {
//          res.render("add",{checkmsg:true})
//       } else {
//          let query2 = "insert into registration_form values(?,?,?,?,?)";
//          conn.query(query2, [name, email, phone, gender, address], (err, result2) => {
//             if (result2.affectedRows > 0) {
//                res.render("add",{msg:true})
//             }
//          })
//       }
//    }
// })

// })


// search user
// app.get("/searchuser",(req,res)=>{
//    // res.send(req.query)
//    const email = req.query.email;
//    // console.log(email);
//    let qry= "select * from registration_form where email=?";
//    con.query(qry,[email],(err,result)=>{
//       if(result.length>0){
//          res.send("search successfully");
//       }
//       else{
//          res.send(err);
//       }
//    })
// })



// create server 
app.listen(port, (err) => {
   if (err) {
      throw err
   } else {
      console.log("server is running on port 3000");
   }
})