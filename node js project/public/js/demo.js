function validation(){
	var return_name;

	var user = document.getElementById("name").value;
	var pattern=/^\w[A-Za-z\s]+$/;  
	console.log("helpme",user)
	if(pattern.test(user) == false || user=="" ){
		document.getElementById("username").innerHTML="** enter valid name";
		 document.getElementById('val_submit').disabled=true;
		 return_name=false
	}else{
        document.getElementById("username").innerHTML="";
         document.getElementById('val_submit').disabled=false;
         return_name=true
	}
	return return_name;

}

function validatemail() {
    var return_email;
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var inputText = document.getElementById("mail").value;


    if (mailformat.test(inputText) == false) {


        document.getElementById("validmail").innerHTML = "** please enter valid mail";
        document.getElementById('val_submit').disabled = true;
        return_email = false;
    } else {
        // document.getElementById("validmail").innerHTML =" please enter valid mail";
        document.getElementById("validmail").innerHTML = "";
        document.getElementById('val_submit').disabled = false;
        return_email = true;
    }
    return return_email;
}



function validatepassword(){
	var return_password;

	var passwordinput= document.getElementById("password").value
	var spattern=/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$!%*?&])[A-Za-z\d@#$!^%*?&]{8,}$/;
 	// fill password
 	if(passwordinput =="") {  
     	document.getElementById("password1").innerHTML = "**Fill the password please!"; 
     	 document.getElementById('val_submit').disabled=true; 
        return_password=false
     }
    // maximum password length
	   if(passwordinput.length > 15) {  
	   document.getElementById("password1").innerHTML = "**Password length must not exceed 15 characters";  
	    document.getElementById('val_submit').disabled=true;
	    return_password=false
	 
	 }

	 if(spattern.test(passwordinput) == false){
    	document.getElementById('password1').innerHTML="*password should have one capital letter one special character one digit ";
    	document.getElementById('password1').style.borderColor = "red";
    	document.getElementById('password1').style.color = "red";
     	document.getElementById('val_submit').disabled=true;
     	return_password=false
     }
     
     else{
     	document.getElementById("password1").innerHTML= "right password"
     	document.getElementById('password').style.borderColor = "green";
     	document.getElementById('password1').style.color = "green";
     	 document.getElementById('val_submit').disabled=false;
     	 return_password=true
     }
     return return_password      

         
	}
	 function val_sub() {
	 	var return_name= validation();
	 	var return_password = validatepassword();

	 	if(return_name==true && return_password==true){
	 		document.getElementById("val_submit").removeAttribute("disabled")
	 	}else{
	 		document.getElementById("val_submit").disabled=true;
	 	}
	 }
