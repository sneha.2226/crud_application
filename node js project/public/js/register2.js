// ------------------------FirstName------------------
function validatename() {
    var return_name;
    var name = document.getElementById('name').value;
    var pattern = /^\w[A-Za-z\s]+$/;
    if (pattern.test(name) == false || name == "") {
        document.getElementById("user").innerHTML = "**enter valid name"
        return_name = false

    }
    // if(name ==""){
    //     document.getElementById("user").innerHTML="**please enter name"
    // }
    else {
        document.getElementById('user').innerHTML = "";
        document.getElementById('user').style.color = "";
        return_name = true
    }
    return return_name;
}
//-------------------------LastName---------------------  
function validatelname() {
    var return_name;
    var name = document.getElementById('lastname').value;
    var pattern = /^\w[A-Za-z\s]+$/;
    if (pattern.test(name) == false || name == "") {
        document.getElementById("username").innerHTML = "**enter valid name"
        return_name = false

    }
    // if(name ==""){
    //     document.getElementById("user").innerHTML="**please enter name"
    // }
    else {
        document.getElementById('username').innerHTML = "";
        document.getElementById('username').style.color = "";
        return_name = true
    }
    return return_name;
}

//-------------------------Email---------------------------

function validatemail() {
    var return_email;
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var inputText = document.getElementById("mail").value;


    if (mailformat.test(inputText) == false) {


        document.getElementById("validmail").innerHTML = "** please enter valid mail";
        document.getElementById('val_submit').disabled = true;
        return_email = false;
    } else {
        // document.getElementById("validmail").innerHTML =" please enter valid mail";
        document.getElementById("validmail").innerHTML = "";
        document.getElementById('val_submit').disabled = false;
        return_email = true;
    }
    return return_email;
}
//--------------------------Phone no---------------------------

function validatephone() {
    var return_phone;

    var phone = /[6789][0-9]{9}/;

    // var phoneno = document.getElementById("phone").value;
    var phoneno = document.getElementById("phone").value;



    if (phone.test(phoneno) == true) {
        document.getElementById("phone1").innerHTML = "valid"
        document.getElementById('phone1').style.color = "green";
        document.getElementById('val_submit').disabled = false;
        return_phone = true

    } else {
        document.getElementById("phone1").innerHTML = "invalid number";
        document.getElementById("phone1").style.color = "red";
        document.getElementById('val_submit').disabled = true;
        return_phone = false;

    }
    return return_phone;
}


//-----------------------------Address--------------------------
function validateaddress() {
    var return_address
    var address = document.getElementById("address").value

    if (address == "") {
        document.getElementById("address1").innerHTML = "**please enter Address";
        document.getElementById('val_submit').disabled = true;
        return_address = false;
    } else {
        document.getElementById("address1").innerHTML = ""
        return_address = true
    }

    return return_address;
}






//------------------------------Password---------------------------
function validatepassword() {
    var return_password;

    var passwordinput = document.getElementById("password").value
    var spattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$!%*?&])[A-Za-z\d@#$!^%*?&]{8,}$/;
    // fill password
    if (passwordinput == "") {
        document.getElementById("password1").innerHTML = "**Fill the password please!";
        document.getElementById('val_submit').disabled = true;
        return_password = false

    }


    if (spattern.test(passwordinput) == false) {
        document.getElementById('password1').innerHTML = "must have 1uppercase 1digit 1 special character";
        document.getElementById('password1').style.borderColor = "red";
        document.getElementById('password1').style.color = "red";
        document.getElementById('val_submit').disabled = true;
        return_password = false
    } else {
        document.getElementById("password1").innerHTML = "right password"
        document.getElementById('password1').style.borderColor = "green";
        document.getElementById('password1').style.color = "green";
        document.getElementById('val_submit').disabled = false;

        return_password = true;
    }
    return return_password;


}
//-------------------------eye----------------------------------
const togglePassword = document.querySelector('#togglePassword');
const password = document.querySelector('#password');
togglePassword.addEventListener('click', function(e) {
    // toggle the type attribute
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa-eye-slash');
});
//----------------------ConfirmPassword-------------------------

function confirmpassword() {
    var return_confirm

    var password = document.getElementById("password").value
    var password1 = document.getElementById("confirmpass").value
    if (password != password1) {
        document.getElementById("confirmpass1").innerHTML = "**password did not match"
        document.getElementById('confirmpass1').style.color = "red";
        document.getElementById('confirmpass1').style.borderColor = "red";
        document.getElementById('val_submit').disabled = true;
        return_confirm = false;

    } else {
        document.getElementById("confirmpass1").innerHTML = "password matched"
        document.getElementById('confirmpass1').style.color = "green";
        document.getElementById('confirmpass1').style.borderColor = "green";
        document.getElementById('val_submit').disabled = false;
        return_confirm = true;
    }
    if (password==password1) {

    document.getElementById("confirmpass1").innerHTML = "";
    document.getElementById('confirmpass').style.borderColor = "#ced4da";
    confirmPasswordError=true
  } else {
    document.getElementById('confirmpass1').style.borderColor = 'red';
    document.getElementById('confirmpass').innerHTML="*not match!!";
    confirmPasswordError=false;
  }
    return return_confirm;
}
//-------------------------PIN-----------------------------------

function isUSAZipCode(str) {
    return /^\d{6}(-\d{5})?$/.test(str);
}

function validateInput() {
    // console.log("validateInput");
    var retrun_pin
    var pattern = /^\d{6}(-\d{5})?$/
    let zipCode = document.getElementById("pin").value;

    if (isUSAZipCode(zipCode)) {
        document.getElementById("msg").innerHTML = ""
        document.getElementById("msg").style.color = "green"
        document.getElementById('val_submit').disabled = false;
        return_pin = true;
    } else {
        document.getElementById("msg").innerHTML = "invalid PIN";
        document.getElementById("msg").style.color = "red"
        document.getElementById('val_submit').disabled = true;
        return_pin = false
    }
    return return_pin

}

function val_sub() {
    // console.log("1")
    var return_name = validatename();
    var return_email = validatemail();
    var return_phone = validatephone();
    var return_address = validateaddress();
    var return_password = validatepassword();
    var return_confirm = confirmpassword();
    var retrun_pin = validateInput();




    console.log("1", return_name)
    console.log("1", return_email)
    console.log("1", return_phone)
    console.log("1", return_address)
    console.log("1", return_password)
    console.log("1", return_confirm)
    console.log("1", retrun_pin)


    if (return_name == true && return_email == true && return_phone == true &&
        return_address == true && return_password == true && return_confirm == true &&
        return_pin == true) {

        document.getElementById("val_submit").removeAttribute('disabled');
    } else {
        document.getElementById("val_submit").disabled = true;


    }

}
$(function() {
    $("input[name='fpin']").on('input', function(e) {
        $(this).val($(this).val().replace(/[^0-9]/g, ''));
    });
});
$(function() {
    $("input[name='number']").on('input', function(e) {
        $(this).val($(this).val().replace(/[^0-9]/g, ''));
    });
});

$(function() {
    $("input[name='address']").on('input', function(e) {
        console.log('==>', $(this).val().charAt(3))
        if ($(this).val().indexOf("  ") != -1) {
            $(this).val($(this).val().replace("  ", " "));
            // $(this).val($(this).val().replace(".", ""));
        } else {
            $(this).val($(this).val().replace(".", ""));
        }
        // if($(this).val().indexOf("  ")) {
        //   $(this).val($(this).val().replace(/[^A-Za-z]/g, ''));
        // } else {
        //   $(this).val($(this).val().replace(/\s+/g,' '));
        //  }
    });
});

$(function() {
    $("input[name='firstname']").on('input', function(e) {
        console.log('==>', $(this).val().charAt(3))
        if ($(this).val().indexOf("  ") != -1) {
            $(this).val($(this).val().replace("  ", " "));
            // $(this).val($(this).val().replace(".", ""));
        } else {
            $(this).val($(this).val().replace(".", ""));
        }
        // if($(this).val().indexOf("  ")) {
        //   $(this).val($(this).val().replace(/[^A-Za-z]/g, ''));
        // } else {
        //   $(this).val($(this).val().replace(/\s+/g,' '));
        //  }
    });
});
// $(function() {
//     $("input[name='email']").on('input', function(e) {
//         console.log('==>', $(this).val().charAt(3))
//         if ($(this).val().indexOf("  ") != -1) {
//             $(this).val($(this).val().replace("  ", " "));
//             // $(this).val($(this).val().replace(".", ""));
//         } else {
//             $(this).val($(this).val().replace(".", ""));
//         }
//         // if($(this).val().indexOf("  ")) {
//         //   $(this).val($(this).val().replace(/[^A-Za-z]/g, ''));
//         // } else {
//         //   $(this).val($(this).val().replace(/\s+/g,' '));
//         //  }
//     });
// });





const fetApi = () => {
    var pincode = document.getElementById("pin").value;
    fetch(`https://api.postalpincode.in/pincode/${pincode}`).then((response) => response.json())
      .then((data) => {
        // console.log('vaaba', data[0].PostOffice[0]);
        var city = data[0].PostOffice[0].District;
        var state = data[0].PostOffice[0].State;
        var country = data[0].PostOffice[0].Country;
        document.getElementById("country").value = country;
        document.getElementById("city").value=city;
        document.getElementById("state").value=state;
    });
}

